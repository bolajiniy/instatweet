﻿using System;

namespace Mobile.Notifications
{
    public class Payload
    {
        public int Id { set; get; }
        public string Data { set; get; }
        public bool Ongoing { set; get; }
        public Payload()
        {
            var now = DateTime.Now;
            var id = $"{now.Hour}{now.Minute}{now.Second}{now.Millisecond}";

            if (int.TryParse(id, out int val))
            {
                Id = val;
            }
            else
            {
                Id = 1;
            }
        }
    }
}