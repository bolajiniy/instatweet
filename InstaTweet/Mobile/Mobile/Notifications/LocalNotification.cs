﻿
using Newtonsoft.Json;
using Plugin.LocalNotification;
using Xamarin.Forms;

namespace Mobile.Notifications
{
    public class LocalNotification : ILocalNotification
    {
        public void CancelAll()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                NotificationCenter.Current.CancelAll();
            });
        }

        public void Notify(string title, Payload payload, string content = "")
        {
            if (payload == null)
            {
                payload = new Payload();
            }
            var notification = new NotificationRequest
            {
                NotificationId = payload.Id,
                Title = title,
                Description = content,
                ReturningData = JsonConvert.SerializeObject(payload), // Returning data when tapped on notification.
                Android = new AndroidOptions
                {
                    Ongoing = payload.Ongoing,
                    IconName = "notification_bar_icon"
                }
                //NotifyTime = DateTime.Now.AddSeconds(30) // Used for Scheduling local notification, if not specified notification will show immediately.
            };

            Device.BeginInvokeOnMainThread(() =>
            {
                NotificationCenter.Current.Show(notification);
            });
        }

        public void Notify(string title, string content = "", string url = null)
        {
            Notify(title, new Payload
            {
                //Id = 111,
                Data = url
            }, content);
        }
    }
}