﻿namespace Mobile.Notifications
{
    public interface ILocalNotification
    {
        void Notify(string title, string content = "", string url = null);
        void Notify(string title, Payload payload, string content = "");
        void CancelAll();
    }
}