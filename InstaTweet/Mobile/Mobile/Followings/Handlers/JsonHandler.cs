﻿////using Mobile.Common;
//using Mobile.Db;
//using Mobile.Followings.EvtArgs;
//using Mobile.Models;
//using System;
//using System.Collections.Generic;
//using System.Threading.Tasks;
//using System.IO;
//using Tweetinvi;
//using Mobile.Common;

//namespace Mobile.Followings.Worker
//{
//    public class JsonHandler : IManagerHandler
//    {
//        private string _jsonPathAppDocu;
//        private string _jsonPathAppData;

//        public event EventHandler<OnExportArgs> OnExported;
//        public event EventHandler<OnImportedArgs> OnImported;
//        public event EventHandler<OnLoadedArgs> OnLoaded;

//        public void DoExportToDocuments(List<Followee> followees)
//        {
//            File.WriteAllText(_jsonPathAppDocu, followees.ToJson());

//            var args = new OnExportArgs
//            {
//                Location = _jsonPathAppDocu
//            };

//            OnExported?.Invoke(this, args);
//        }

//        public void DoImportFromDocuments(out List<Followee> followees)
//        {
//            followees = new List<Followee>();

//            if (File.Exists(_jsonPathAppDocu))
//            {
//                var json = File.ReadAllText(_jsonPathAppDocu);
//                followees = json.ConvertJsonTo<List<Followee>>();
//            }

//            var args = new OnImportedArgs
//            {
//                Count = followees.Count,
//                Location = _jsonPathAppDocu
//            };

//            OnImported?.Invoke(this, args);
//        }

//        public void DoInitialize(string screenName)
//        {
//            /*  //throw new NotImplementedException("using Mobile.Common");
//              _jsonPathAppDocu = $"{screenName}.doc";// Path.Combine(FileSystemHelper.AppDocuDirectory, $"{screenName}.json");
//              _jsonPathAppData = $"{screenName}.data";// Path.Combine(FileSystemHelper.AppDataDirectory, $"{screenName}.json");*/
//            _jsonPathAppDocu = Path.Combine(FileSystemHelper.AppDocuDirectory, $"{screenName}_doc.json");
//            _jsonPathAppData = Path.Combine(FileSystemHelper.AppDocuDirectory, $"{screenName}_data.json");
//        }

//        public async Task<List<Followee>> DoLoadFromAppData()
//        {
//            var list = await AppDatabase.Instance.GetData<List<Followee>>(_jsonPathAppData);

//            var args = new OnLoadedArgs
//            {
//                Count = list.Count
//            };

//            OnLoaded?.Invoke(this, args);

//            return list;
//        }

//        public void DoSaveToAppData(List<Followee> followees)
//        {
//            File.WriteAllText(_jsonPathAppData, followees.ToJson());
//        }

//        List<Followee> IManagerHandler.DoLoadFromAppData()
//        {
//            throw new NotImplementedException();
//        }
//    }
//}