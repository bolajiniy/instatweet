﻿using System;

namespace Mobile.Followings.EvtArgs
{
    public class OnLoadedArgs : EventArgs
    {
        public int Count { get; set; }
    }
}