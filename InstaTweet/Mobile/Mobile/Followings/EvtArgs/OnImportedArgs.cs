﻿using System;

namespace Mobile.Followings.EvtArgs
{
    public class OnImportedArgs : EventArgs
    {
        public int Count { get; set; }
        public string Location { get; set; }
    }
}