﻿using System;

namespace Mobile.Followings.EvtArgs
{
    public class OnExportArgs : EventArgs
    {
        public string Location { get; set; }
    }
}