﻿using Mobile.Followings.EvtArgs;
using Mobile.Models;
using System;
using System.Collections.Generic;

namespace Mobile.Followings
{
    public interface IManagerHandler
    {
        event EventHandler<OnExportArgs> OnExported;
        event EventHandler<OnImportedArgs> OnImported;
        event EventHandler<OnLoadedArgs> OnLoaded;

        void DoInitialize(string screenName);
        List<Followee> DoLoadFromAppData();
        void DoSaveToAppData(List<Followee> followees);
        void DoImportFromDocuments(out List<Followee> followees);
        void DoExportToDocuments(List<Followee> followees);
    }
}