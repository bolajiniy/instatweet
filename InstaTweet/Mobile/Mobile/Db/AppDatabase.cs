﻿using SQLite;
using System;
using System.IO;
using System.Collections.Generic;
using Mobile.Models;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace Mobile.Db
{
    public class AppDatabase
    {

        private static readonly AppDatabase instance = new AppDatabase();


        public readonly SQLiteAsyncConnection con;

        public static AppDatabase Instance { get => instance; }

        private AppDatabase()
        {
            con = new SQLiteAsyncConnection(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "app.db"));
            Init();
        }

        private async void Init()
        {
            await con.CreateTableAsync<DataRepo>();
            await con.CreateTableAsync<Followee>();
        }

        public async void SaveData<T>(T data, string key = null) where T : class
        {
            string _key = key;

            if (string.IsNullOrEmpty(_key))
            {
                _key = typeof(T).Name;
            }

            
            await con.InsertOrReplaceAsync(new DataRepo
            {
                key = _key,
                Data = JsonConvert.SerializeObject(data)
            });
        }

        public async Task<List<Followee>> GetUserFolloweesAsync(long userId)
        {
            return await con.Table<Followee>().Where(t => t.UserId == userId).ToListAsync();
        }

        public async Task<Followee> GetUserFolloweeAsync(long id, long userId)
        {
            return await con.Table<Followee>().FirstOrDefaultAsync(t => t.Id == id && t.UserId ==userId);
        }

        public async Task SaveFolloweeAsync(Followee followee)
        {
            await con.InsertOrReplaceAsync(followee);
        }


        public async Task SaveFolloweeAsync(List<Followee> followees)
        {
            await con.Table<Followee>().DeleteAsync(t => t.UserId == App.AuthenticatedUser.Id);
            foreach (var f in followees)
            {
                await SaveFolloweeAsync(f);
            }
        }


        public async Task DeleteUserFolloweeAsync(long id, long userId)
        {
            await con.Table<Followee>().DeleteAsync(t => t.Id == id && t.UserId == userId);
        }

        public async Task<T> GetData<T>(string key = null) where T : class
        {
            try
            {
                string _key = key;

                if (string.IsNullOrEmpty(_key))
                {
                    _key = typeof(T).Name;
                }

                var repo = await con.Table<DataRepo>().FirstAsync(d => d.key == _key);

                return JsonConvert.DeserializeObject<T>(repo.Data);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        public async void Delete<T>(string key = null) where T : class
        {
            if (string.IsNullOrEmpty(key))
            {
                key = typeof(T).Name;
            }

            await con.Table<DataRepo>().DeleteAsync(d => d.key == key);

        }
    }
}
