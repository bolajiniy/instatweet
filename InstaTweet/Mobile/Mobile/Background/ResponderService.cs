﻿using Mobile.Db;
using Mobile.Models;
using Mobile.Notifications;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Tweetinvi;
using Tweetinvi.Events;
using Tweetinvi.Models;
using Tweetinvi.Parameters;
using Tweetinvi.Streaming;
using Xamarin.Essentials;
using Xamarin.Forms.Background;

namespace Mobile.Background
{
    public class ResponderService : IBackgroundTask
    {
        private ILocalNotification localNotification;

        private List<Followee> followees;

        private IFilteredStream filteredStream;

        private bool _isStreamActive => filteredStream.StreamState == StreamState.Running;

        public bool IsStarted = false;

        public event EventHandler<bool> OnStatusChange;

        TimeSpan delay = TimeSpan.FromSeconds(5);
         private CancellationTokenSource tokenSource = new CancellationTokenSource();

        public static ResponderService Instance { get; } = new ResponderService();

        private ResponderService()
        {
            localNotification = new LocalNotification();
            InitConnectivity();
        }
        public Task StartJob()
        {

            return Task.Run(async () =>
           {
               while (true)
               {
                   if (IsStarted)
                   {
                       try
                       {
                           InitFollowers();
                           Console.WriteLine("Start Job....");
                           filteredStream.StartStreamMatchingAllConditions();
                           Console.WriteLine("End Job....");
                       }
                       catch (Exception ex)
                       {
                           Console.WriteLine(ex);
                       }
                   }
                   else
                   {
                       Console.WriteLine("not started....");
                   }

                   Console.WriteLine("delay starting....");

                   await Delay();

                   Console.WriteLine("delay done....");
               }


           });


        }


        private   async Task Delay()
        {
            try
            {
                await Task.Delay(delay, tokenSource.Token);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private   void CancelDelay()
        {
            tokenSource.Cancel();
            tokenSource = new CancellationTokenSource();
        }

        private async void InitFollowers()
        {
            try
            {
                followees = await AppDatabase.Instance.GetUserFolloweesAsync(App.AuthenticatedUser.Id);
                filteredStream.ClearFollows();

                foreach (var followee in followees)
                {
                    filteredStream.AddFollow(followee.Id);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void InitStream()
        {
            if (filteredStream == null)
            {
                filteredStream = Stream.CreateFilteredStream();
                filteredStream.MatchingTweetReceived += TweetReceived;
                filteredStream.StreamStarted += StreamStarted;
                filteredStream.StreamStopped += StreamStopped;
            }
        }

        private void StreamStopped(object sender, StreamExceptionEventArgs e)
        {
            Console.WriteLine("StreamStopped");
            Console.WriteLine(e.DisconnectMessage);
            Console.WriteLine(e.Exception);
            // IsServiceRunning = false;
        }

        private void StreamStarted(object sender, EventArgs e)
        {
            Console.WriteLine("StreamStarted");
            //IsServiceRunning = true;
        }

        private void InitConnectivity()
        {
            Connectivity.ConnectivityChanged += ConnectivityChanged;
        }

        private void ConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
        {
            if (IsStarted)
            {
                switch (e.NetworkAccess)
                {
                    case NetworkAccess.Internet:
                        localNotification.Notify("Connection found.", "Restarted the stream.");

                        delay = TimeSpan.FromSeconds(delay.TotalSeconds / 2);
                        CancelDelay();

                        break;
                    case NetworkAccess.None:
                        localNotification.Notify("Connection lost.", "Will restart the stream when connection is back.");
                        delay = TimeSpan.FromSeconds(delay.TotalSeconds * 2);
                        break;
                }
            }
        }



        private void TweetReceived(object sender, MatchedTweetReceivedEventArgs e)
        {
            try
            {
                var tweet = e.Tweet;
                Console.WriteLine(e.Tweet.FullText);

                if (IsNotRetweetAndReply(tweet))
                {
                    var followee = followees.SingleOrDefault(x => x.Id.Equals(tweet.CreatedBy.Id));

                    if (followee == null) return;

                    var parameters = CreateReplyParameters(tweet, followee);

                    localNotification.Notify($"{tweet.CreatedBy.ScreenName} tweeted.", tweet.Text, tweet.Url);

                    Tweet.PublishTweet(parameters);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private bool IsNotRetweetAndReply(ITweet tweet)
        {
            return (!tweet.IsRetweet && (tweet.InReplyToScreenName == null && tweet.InReplyToStatusId == null));
        }

        private PublishTweetParameters CreateReplyParameters(ITweet tweet, Followee followee)
        {

            var tweetParams = new PublishTweetParameters($"@{tweet.CreatedBy.ScreenName} {followee.ReplyBody}")
            {
                InReplyToTweetId = tweet.Id
            };

            if (!string.IsNullOrWhiteSpace(followee.Data))
            {
                byte[] imgBytes = JsonConvert.DeserializeObject<byte[]>(followee.Data);
                tweetParams.MediaBinaries.Add(imgBytes);
            }

            return tweetParams;

        }


        public void Start()
        {
            try
            {
                InitStream();

                localNotification.Notify("Started",
                    new Payload
                    {
                        Id = 100,
                        Ongoing = true
                    }, "Waiting for Tweets...");

                IsStarted = true;

                OnStatusChange?.Invoke(this, IsStarted);

                delay = TimeSpan.FromSeconds(5);

                CancelDelay();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public void Stop()
        {
            try
            {

                if (filteredStream.StreamState != StreamState.Stop)
                    filteredStream.StopStream();

                IsStarted = false;


                OnStatusChange?.Invoke(this, IsStarted);

                delay = TimeSpan.FromMinutes(30);

                localNotification.CancelAll();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
