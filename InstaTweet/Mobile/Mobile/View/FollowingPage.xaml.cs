﻿using Mobile.ViewModel;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mobile.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FollowingPage : ContentPage
    {
        private readonly FollowingViewModel viewModel;
        public FollowingPage()
        {
            InitializeComponent();
            BindingContext = viewModel= new FollowingViewModel();
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if(sender is ListView listView)
            {
                listView.SelectedItem = null;
            }
        }

        private void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            
        }
    }
}