﻿using Newtonsoft.Json;
using PropertyChanged;
using SQLite;
using System;
using System.IO;
using Xamarin.Forms;

namespace Mobile.Models
{
    [AddINotifyPropertyChangedInterface]
    public class Followee
    {
        [PrimaryKey]
        public long Id { get; set; }
        [NotNull]
        public long UserId { set; get; }
        [NotNull]
        public string ScreenName { get; set; }
        [NotNull]
        public string ReplyBody { get; set; }
        [JsonIgnore]
        public string Data { get;  set; }

        [Ignore]
        [JsonIgnore]
        public ImageSource Image
        {
            get
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(Data))
                    {
                        return null;
                    }
                    else
                    {
                        var bytes = JsonConvert.DeserializeObject<byte[]>(Data);

                        return ImageSource.FromStream(() =>
                        {
                            return new MemoryStream(bytes);
                        });
                    }
                }
                catch( Exception ex)
                {
                    Console.WriteLine(ex);
                    return null;
                }
            }
        }
    }
}