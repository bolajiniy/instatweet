﻿using SQLite;

namespace Mobile.Models
{
    public class DataRepo
    {
        [PrimaryKey]
        public string key { set; get; }
        public string Data { set; get; }
    }
}