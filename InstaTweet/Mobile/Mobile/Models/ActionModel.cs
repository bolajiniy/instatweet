﻿using PropertyChanged;
using Rg.Plugins.Popup.Services;
using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace Mobile.Models
{
    [AddINotifyPropertyChangedInterface]
    public class ActionModel
    {
        public Action Action { set; get; }
        public ICommand Command => new Command(async () =>
        {
            if (PopupNavigation.Instance.PopupStack.Count > 0)
                await PopupNavigation.Instance.PopAsync();

            Action?.Invoke();

        });
        public string Title { set; get; }
    }
}