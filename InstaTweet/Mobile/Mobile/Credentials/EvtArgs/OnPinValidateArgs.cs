﻿using System;
using Tweetinvi.Models;

namespace Mobile.Credentials.EvtArgs
{
    public class OnPinValidateArgs : EventArgs
    {
        public ITwitterCredentials TwitterCredential { get; set; }
    }
}