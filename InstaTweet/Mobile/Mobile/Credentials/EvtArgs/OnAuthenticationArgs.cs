﻿using System;

namespace Mobile.Credentials.EvtArgs
{
    public class OnAuthenticationArgs : EventArgs
    {
        public string AuthenticationUrl { get; set; }
    }
}