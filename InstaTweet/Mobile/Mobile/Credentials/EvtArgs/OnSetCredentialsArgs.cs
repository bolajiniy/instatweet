﻿using System;

namespace Mobile.Credentials.EvtArgs
{
    public class OnSetCredentialsArgs : EventArgs
    {
        public bool IsAuthenticated { get; set; }
    }
}