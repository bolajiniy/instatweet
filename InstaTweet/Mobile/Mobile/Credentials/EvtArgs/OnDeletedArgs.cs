﻿using System;

namespace Mobile.Credentials.EvtArgs
{
    public class OnDeletedArgs : EventArgs
    {
        public bool IsDeleted { get; set; }
    }
}