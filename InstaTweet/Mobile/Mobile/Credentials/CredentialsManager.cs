﻿using Mobile.Credentials.EvtArgs;
using Mobile.Db;
using System;
using System.Threading.Tasks;
using Tweetinvi;
using Tweetinvi.Models;
using Xamarin.Essentials;

namespace Mobile.Credentials
{
    public sealed class CredentialsManager
    {
        private IAuthenticationContext _context;

        public event EventHandler<OnAuthenticationArgs> OnAuthentication;
        public event EventHandler<OnPinValidateArgs> OnPinValidate;
        public event EventHandler<OnSetCredentialsArgs> OnCredentialsSet;
        public event EventHandler<OnDeletedArgs> OnDeleted;

        public static CredentialsManager Instance { get; } = new CredentialsManager();

        private CredentialsManager()
        {
            //_filePath = Path.Combine(GetInternalPath(), FILE_NAME);
        }

        public void SetupCredentials()
        {
            try
            {
                var consumerCreds = new ConsumerCredentials
                (
                    "HlMbFePML1mgpoHF89TmzHYsm",
                    "uR4V2g7AWbxHJE9BcXbukBOno1UBZ4KbWEMuOLPI6pIfvgqAK1"
                );

                //todo set app call back url
                _context = AuthFlow.InitAuthentication(consumerCreds);

                OnAuthentication?.Invoke(this, new OnAuthenticationArgs
                {
                    AuthenticationUrl = _context.AuthorizationURL
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public async void SetCredentials()
        {
            var credentials = await LoadCredentialsAsync();


            if (credentials != null)
            {
                Auth.SetCredentials(credentials);

                App.AuthenticatedUser = await UserAsync.GetAuthenticatedUser();
            }

            OnCredentialsSet?.Invoke
            (
                this,
                new OnSetCredentialsArgs
                {
                    IsAuthenticated = App.AuthenticatedUser != null
                }
            );
        }

        public async Task ValidatePinCode(string PinCode)
        {
            var userCredentials = await CreateCredentialsFromVerifierCodeAsync(PinCode);

            if (userCredentials != null)
            {
                Auth.SetCredentials(userCredentials);
                App.AuthenticatedUser = await UserAsync.GetAuthenticatedUser();

                SaveCredentials(userCredentials);
            }

            OnPinValidate?.Invoke
            (
                this,
                new OnPinValidateArgs
                {
                    TwitterCredential = userCredentials
                }
            );
        }

        private async Task<ITwitterCredentials> CreateCredentialsFromVerifierCodeAsync(string PinCode)
        {
            return await Task.Run(() => AuthFlow.CreateCredentialsFromVerifierCode(PinCode, _context));
        }

        public void SaveCredentials(ITwitterCredentials credentials)
        {
            //File.WriteAllText(_filePath, credentials.ToJson());
            AppDatabase.Instance.SaveData(credentials, "ITwitterCredentials");
        }

        public async Task<ITwitterCredentials> LoadCredentialsAsync()
        {

            return await AppDatabase.Instance.GetData<TwitterCredentials>("ITwitterCredentials");

        }

        public async void DeleteCredentials()
        {

            var cred = await LoadCredentialsAsync();

            if (cred != null)
            {
                AppDatabase.Instance.Delete<ITwitterCredentials>("ITwitterCredentials");
            }

            OnDeleted?.Invoke(this,
                new OnDeletedArgs
                {
                    IsDeleted = cred == null
                });

        }

        private string GetInternalPath()
        {
            return FileSystem.AppDataDirectory;
        }
    }
}