﻿using System;
using Mobile.Background;
using Mobile.Credentials;
using Mobile.Notifications;
using Mobile.View;
using Newtonsoft.Json;
using Plugin.LocalNotification;
using Tweetinvi.Models;
using Xamarin.Forms;
using Xamarin.Forms.Background;

namespace Mobile
{
    public partial class App : Application
    {

        public static IAuthenticatedUser AuthenticatedUser = null;
        public App()
        {
            InitializeComponent();

            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            // Local Notification tap event listener
            NotificationCenter.Current.NotificationTapped += OnLocalNotificationTapped;

            CredentialsManager.Instance.SetCredentials();

            BackgroundAggregatorService.Add(() =>
            {
                return ResponderService.Instance;
            });

            BackgroundAggregatorService.Instance.Start();
        }

        private void OnLocalNotificationTapped(NotificationTappedEventArgs e)
        {
            Console.WriteLine(e.Data);

            try
            {
                var payload = JsonConvert.DeserializeObject<Payload>(e.Data);

                if (payload.Data.StartsWith("http"))
                {
                    Device.OpenUri(new Uri(payload.Data));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
