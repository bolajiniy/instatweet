﻿using Acr.UserDialogs;
using Mobile.Db;
using Mobile.Models;
using Mobile.View;
using Newtonsoft.Json;
using Plugin.Media;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Input;
using Tweetinvi;
using Xamarin.Forms;

namespace Mobile.ViewModel
{
    [AddINotifyPropertyChangedInterface]
    public class FollowingViewModel
    {
        public Followee Followee { set; get; } = new Followee();
        public List<Followee> Followees { set; get; } = new List<Followee>();

        public ImageSource ContentImage
        {
            get
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(Followee.Data))
                    {
                        return ImageSource.FromFile("img_preview.png");
                    }
                    else
                    {
                        return ImageSource.FromStream(() =>
                        {
                            var bytes = JsonConvert.DeserializeObject<byte[]>(Followee.Data);
                            return new MemoryStream(bytes);
                        });
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    return ImageSource.FromFile("img_preview.png");
                }
            }
        }
        public Followee SelectedItem
        {
            set
            {
                if (value != null)
                {
                    Followee = new Followee
                    {
                        Id = value.Id, 
                        ReplyBody = value.ReplyBody,
                        ScreenName = value.ScreenName,
                        UserId = value.UserId,
                        Data = value.Data
                    };
                }
            }
        }
        public FollowingViewModel()
        {
            Init();
        }

        private async void Init()
        {
            Followees = await AppDatabase.Instance.GetUserFolloweesAsync(App.AuthenticatedUser.Id);
        }

        public ICommand AddCommand => new Command(async () =>
        {
            if (string.IsNullOrWhiteSpace(Followee.ScreenName) || string.IsNullOrWhiteSpace(Followee.ReplyBody))
            {
                await UserDialogs.Instance.AlertAsync("All fields are required");
            }
            else
            {
                if (Followees.Exists(x => x.ScreenName.Equals(Followee.ScreenName)))
                {
                    UserDialogs.Instance.Toast($"{Followee.ScreenName} already in list.");
                    return;
                }

                try
                {
                    var account = await UserAsync.GetUserFromScreenName(Followee.ScreenName);

                    if (account == null)
                    {
                        throw new Exception();
                    }

                    Followee.Id = account.Id;
                    Followee.UserId = App.AuthenticatedUser.Id;

                    Followees.Add(Followee);

                    Followees = Followees.ToList();

                    Followee = new Followee();
                }
                catch
                {
                    UserDialogs.Instance.Toast($"{Followee.ScreenName} not found.");
                }
            }
        });
        public ICommand RemoveCommand => new Command(() =>
      {
          var item = Followees.FirstOrDefault(s => s.Id == Followee.Id);
          if (item != null)
          {
              Followees.Remove(item);
              Followees = Followees.ToList();
          }
      });
        public ICommand SaveCommand => new Command(async () =>
        {
            using (UserDialogs.Instance.Loading())
            {
                try
                {
                    await AppDatabase.Instance.SaveFolloweeAsync(Followees);
                    MessagingCenter.Send(Followees, "Followees");
                }
                catch (Exception ex)
                {
                    UserDialogs.Instance.Toast(ex.Message);
                }
            }
        });
        public ICommand AttachPhotoCommand => new Command(async () =>
        {

            var status = await CrossPermissions.Current.CheckPermissionStatusAsync<StoragePermission>();

            if (status != PermissionStatus.Granted)
            {
                status = await CrossPermissions.Current.RequestPermissionAsync<StoragePermission>();
            }

            if (status == PermissionStatus.Granted)
            {
                var selectedPhoto = await CrossMedia.Current.PickPhotoAsync();

                if (selectedPhoto == null)
                {
                    Followee.Data = null;
                }
                else
                {
                    var streamPhoto = selectedPhoto.GetStream();

                    MemoryStream mem = new MemoryStream();
                    await streamPhoto.CopyToAsync(mem);

                    mem.Position = 0;

                    Followee.Data = JsonConvert.SerializeObject(mem.ToArray());

                    SelectedItem = Followee;
                }
            }
            else
            {
                UserDialogs.Instance.Toast("Permission denied");
            }

        });

        public ICommand PopupCommand => new Command(async () =>
        {
            var actions = new List<ActionModel>
            {
                new ActionModel
                {
                    Title = "Export Followees",
                    Action = async() =>
                    {
                        try{
                            if (Followees.Count > 0)
                            {

                                string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

                                string filename = Path.Combine(path,  $"{App.AuthenticatedUser.ScreenName}.json");

                                using (var streamWriter = new StreamWriter(filename, true))
                                {
                                    await   streamWriter.WriteLineAsync(JsonConvert.SerializeObject( Followees));
                                }

                                UserDialogs.Instance.Toast($"Followees exported to {filename}", TimeSpan.FromSeconds(3));

                            }
                            else
                            {
                                 UserDialogs.Instance.Toast("No followee to export", TimeSpan.FromSeconds(3));
                            }
                        }
                        catch( Exception ex)
                        {
                            UserDialogs.Instance.Toast(ex.Message, TimeSpan.FromSeconds(3));
                        }
                    }
                },
                 new ActionModel
                {
                    Title = "Import Followees",
                    Action =async () =>
                    {
                         try{

                                string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

                                string filename = Path.Combine(path,  $"{App.AuthenticatedUser.ScreenName}.json");


                            if(File.Exists(filename)){
                                using (var streamReader = new StreamReader(filename))
                                {
                                     string content = await streamReader.ReadToEndAsync();

                                    if (!string.IsNullOrWhiteSpace(content))
                                    {
                                        Followees = JsonConvert.DeserializeObject<List<Followee>>(content);
                                    }
                                    else
                                    {
                                         UserDialogs.Instance.Toast($"Followees imported from {filename}", TimeSpan.FromSeconds(3));
                                    }
                                }
                            }
                            else
                            {
                                UserDialogs.Instance.Toast("No followees exported", TimeSpan.FromSeconds(3));
                            }
                                 
                        }
                        catch( Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                            UserDialogs.Instance.Toast(ex.Message, TimeSpan.FromSeconds(3));
                        }
                    }
                }
            };

            await Rg.Plugins.Popup.Services.PopupNavigation.Instance.PushAsync(new PopupPage(actions));

        });
    }
}
