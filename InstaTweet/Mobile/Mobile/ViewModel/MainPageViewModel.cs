﻿using Acr.UserDialogs;
using Mobile.Background;
using Mobile.Credentials;
using Mobile.Credentials.EvtArgs;
using Mobile.Db;
using Mobile.Models;
using Mobile.View;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using Tweetinvi;
using Xamarin.Forms;
using Xamarin.Forms.Background;

namespace Mobile.ViewModel
{
    [AddINotifyPropertyChangedInterface]
    public class MainPageViewModel
    {
        private readonly MainPage mainPage;

        public string ScreenName { set; get; } = "You're signed out";
        public ImageSource ProfileImageSource { set; get; } = ImageSource.FromFile("signed_out_avatar.png");
        public string StartButtonLabel => IsStarted ? "Stop" : "Start";
        public string FollowingButtonLabel { set; get; } = "Followings";
        public string CountLabel { set; get; } = "0 Followees in list.";
        public string PinCode { set; get; }
        public bool NeedPinValidation { set; get; } = true;
        public bool EnableUiControls { set; get; }
        public bool IsStarted { set; get; }

        public MainPageViewModel(MainPage mainPage)
        {
            this.mainPage = mainPage;
            Init(mainPage);
        }

        private void Init(MainPage mainPage)
        {
            CredentialsManager.Instance.OnAuthentication += OnAuthentication;
            CredentialsManager.Instance.OnPinValidate += OnPinValidate;
            CredentialsManager.Instance.OnCredentialsSet += OnCredentialsSet;
            CredentialsManager.Instance.OnDeleted += OnDeleted;

            ResponderService.Instance.OnStatusChange += OnStatusChange;

            MessagingCenter.Subscribe<List<Followee>>(this, "Followees", async (f) =>
            {
                CountLabel = $"{f.Count} Followees in list.";

                if (mainPage.Navigation.ModalStack.Count > 0)
                    await mainPage.Navigation.PopModalAsync(true);
            });
        }

        private void OnDeleted(object sender, OnDeletedArgs e)
        {
            if (e.IsDeleted)
            {
                UserDialogs.Instance.Toast("Credentials already cleared, Please restart InstaTweet.");
            }
            else
            {
                ProfileImageSource = ImageSource.FromFile("signed_out_avatar.png"); ;
                ScreenName = "Signed Out.";
                EnableUiControls = false;

                UserDialogs.Instance.Toast("Credentials have been cleared, Please restart InstaTweet.");

                if (IsStarted)
                    ResponderService.Instance.Stop();
            }
        }

        private void OnStatusChange(object sender, bool e)
        {
            IsStarted = e;
        }

        private async void OnCredentialsSet(object sender, OnSetCredentialsArgs e)
        {

            if (e.IsAuthenticated)
            {
                await LogedIn();
            }
            else
            {
                CredentialsManager.Instance.SetupCredentials();
            }
        }

        private async void OnPinValidate(object sender, OnPinValidateArgs e)
        {

            if (e.TwitterCredential == null)
            {
                UserDialogs.Instance.Toast("Invalid Pin Code.");
            }
            else
            {
                await LogedIn();
            }

        }

        private async Task LogedIn()
        {
            using (UserDialogs.Instance.Loading("Please Wait..."))
            {
                var authenticatedUser = App.AuthenticatedUser;// await UserAsync.GetAuthenticatedUser();

                NeedPinValidation = false;

                //App.AuthenticatedUser = authenticatedUser;

                ScreenName = $"Signed in - {authenticatedUser.ScreenName}";

                var imgStream = await authenticatedUser.GetProfileImageStreamAsync();

                ProfileImageSource = ImageSource.FromStream(() =>
                {
                    return imgStream;
                });

                EnableUiControls = true;

                var followees = await AppDatabase.Instance.GetUserFolloweesAsync(App.AuthenticatedUser.Id);
                MessagingCenter.Send(followees, "Followees");
            }
        }

        private void OnAuthentication(object sender, OnAuthenticationArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Device.OpenUri(new Uri(e.AuthenticationUrl));
            });
        }

        public ICommand VerifyPinCommand => new Command(async () =>
        {
            if (string.IsNullOrEmpty(PinCode))
            {
                UserDialogs.Instance.Toast("Kindly enter valid pin from twitter");
            }
            else
            {
                using (UserDialogs.Instance.Loading("Verifying Pin, Please Wait..."))
                {
                    await CredentialsManager.Instance.ValidatePinCode(PinCode);
                }
            }
        });
        public ICommand StartCommand => new Command(() =>
       {
           if (IsStarted)
               ResponderService.Instance.Stop();
           else
               ResponderService.Instance.Start();

       });

        public ICommand FollowingCommand => new Command(async () =>
        {
            await mainPage.Navigation.PushModalAsync(new FollowingPage());
        });
        public ICommand PopupCommand => new Command(async () =>
        {
            var actions = new List<ActionModel>
            {
                new ActionModel
                {
                    Title = "Log Out",
                    Action = () =>
                    {
                        CredentialsManager.Instance.DeleteCredentials();
                    }
                }
            };
            await Rg.Plugins.Popup.Services.PopupNavigation.Instance.PushAsync(new PopupPage(actions));
             
        });
    }
}
