﻿using Xamarin.Essentials;
using Android.OS;

namespace InstaTweet.Common
{
    public class FileSystemHelper
    {
        public static string AppDocuDirectory => 
            Environment.GetExternalStoragePublicDirectory(Environment.DirectoryDownloads).Path;

        public static string AppDataDirectory => 
            FileSystem.AppDataDirectory;
    }
}