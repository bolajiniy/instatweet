﻿using Android.OS;

namespace InstaTweet.Common
{
    public class ResponderManager
    {
        public static bool IsServiceRunning { get; set; } = false;
        public static bool IsOreoOrGreater => Build.VERSION.SdkInt >= BuildVersionCodes.O;
    }
}