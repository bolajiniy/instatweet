﻿using Android.App;
using Android.Views;
using Android.Widget;
using InstaTweet.Extensions;
using InstaTweet.Models;
using System.Collections.Generic;

namespace InstaTweet.Adapters
{
    public class FolloweesAdapter : BaseAdapter<Followee>
    {
        private List<Followee> _followees;
        private Activity _context;

        public FolloweesAdapter(List<Followee> followees, Activity context) : base()
        {
            _followees = followees;
            _context = context;
        }

        public override int Count => _followees.Count;

        public override Followee this[int position] => _followees[position];

        public override long GetItemId(int position) => _followees[position].Id;

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView;

            if (view == null)
            {
                view = _context.LayoutInflater.Inflate(Resource.Drawable.SimpleListView3, null);
            }

            var followee = this[position];

            if (followee.Photo.IsAttached)
            {
                var smallPreview = followee.Photo.EncodedBase64.ToBitmap();

                view.FindViewById<ImageView>(Resource.Id.SmallPreview).SetImageBitmap(smallPreview);
            }

            view.FindViewById<TextView>(Resource.Id.ScreenName).Text = followee.ScreenName;
            view.FindViewById<TextView>(Resource.Id.ReplyBody).Text = followee.ReplyBody;

            return view;
        }
    }
}