﻿using Android.App;
using Android.Content;
using System;

namespace InstaTweet.Services
{
    public class ServiceManagerO : IServiceManager
    {
        private readonly Context context;

        public ServiceManagerO()
        {
            context = Application.Context;
        }

        public event EventHandler Started;
        public event EventHandler Stopped;

        private void RaiseServiceEvent(EventHandler EventType)
        {
            EventType?.Invoke(this, new EventArgs());
        }

        public void StartService(Intent serviceIntent)
        {
            context.StartForegroundService(serviceIntent);

            RaiseServiceEvent(Started);
        }

        public void StopService(Intent serviceIntent)
        {
            context.StopService(serviceIntent);

            RaiseServiceEvent(Stopped);
        }
    }
}