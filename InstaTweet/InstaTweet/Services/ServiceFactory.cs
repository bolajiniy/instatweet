﻿using InstaTweet.Common;

namespace InstaTweet.Services
{
    public class ServiceFactory
    {
        public static ServiceFactory Instance { get; } = new ServiceFactory();

        public IServiceManager ServiceManager { get; }

        private ServiceFactory()
        {
            if (ResponderManager.IsOreoOrGreater)
            {
                ServiceManager = new ServiceManagerO();
            }
            else
            {
                ServiceManager = new ServiceManager();
            }
        }
    }
}