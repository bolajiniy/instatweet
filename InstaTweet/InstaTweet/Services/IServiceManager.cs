﻿using Android.Content;
using System;

namespace InstaTweet.Services
{
    public interface IServiceManager
    {
        event EventHandler Started;
        event EventHandler Stopped;
        void StartService(Intent serviceIntent);
        void StopService(Intent serviceIntent);
    }
}