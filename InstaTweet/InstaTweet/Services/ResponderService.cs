﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using InstaTweet.Common;
using InstaTweet.Extensions;
using InstaTweet.Models;
using InstaTweet.Notifications;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Tweetinvi;
using Tweetinvi.Events;
using Tweetinvi.Models;
using Tweetinvi.Parameters;
using Tweetinvi.Streaming;
using Xamarin.Essentials;

namespace InstaTweet.Services
{
    [Service]
    public class ResponderService : Service
    {
        private ILocalNotification localNotification;

        private List<Followee> followees;

        private IFilteredStream filteredStream;
        private BackgroundWorker backgroundStreamWorker;

        private bool isStreamActive => filteredStream.StreamState == StreamState.Running;

        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        [return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
        {
            base.OnStartCommand(intent, flags, startId);

            followees = intent.GetStringExtra("followees").ConvertJsonTo<List<Followee>>();

            addFolloweesToStream(followees);

            backgroundStreamWorker.RunWorkerAsync();

            ResponderManager.IsServiceRunning = true;

            return StartCommandResult.Sticky;
        }

        private void addFolloweesToStream(IEnumerable<Followee> followees)
        {
            filteredStream.ClearFollows();

            foreach (Followee followee in followees)
            {
                filteredStream.AddFollow(followee.Id);
            }
        }

        public override void OnCreate()
        {
            base.OnCreate();

            initFields();

            initWorker();

            initStream();

            initConnectivity();

            initForeground();
        }

        private void initFields()
        {
            localNotification = LocalNotificationFactory.Instance.GetLocalNotification();
        }

        private void initWorker()
        {
            backgroundStreamWorker = new BackgroundWorker();
            backgroundStreamWorker.DoWork += DoWork;
        }

        private void initStream()
        {
            filteredStream = Stream.CreateFilteredStream();
            filteredStream.MatchingTweetReceived += TweetReceived;
        }

        private void initConnectivity()
        {
            Connectivity.ConnectivityChanged += ConnectivityChanged;
        }

        private void initForeground()
        {
            var notification = localNotification.CreateForegroundNotification();

            StartForeground(1, notification);
        }

        private void DoWork(object sender, DoWorkEventArgs e)
        {
            filteredStream.StartStreamMatchingAllConditions();
        }

        private void ConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
        {
            switch (e.NetworkAccess)
            {
                case NetworkAccess.Internet:
                    localNotification.Notify("Connection found.", "Restarted the stream.");
                    if (!backgroundStreamWorker.IsBusy && !isStreamActive)
                    {
                        backgroundStreamWorker.RunWorkerAsync();
                    }
                    break;
                case NetworkAccess.None:
                    localNotification.Notify("Connection lost.", "Will restart the stream when connection is back.");
                    break;
            }
        }

        public override void OnDestroy()
        {
            ResponderManager.IsServiceRunning = false;

            if (isStreamActive)
                filteredStream.StopStream();

            base.OnDestroy();
        }

        private void TweetReceived(object sender, MatchedTweetReceivedEventArgs e)
        {
            var tweet = e.Tweet;

            if (IsNotRetweetAndReply(tweet))
            {
                var followee = followees.SingleOrDefault(x => x.Id.Equals(tweet.CreatedBy.Id));

                if (followee == null) return;

                var parameters = CreateReplyParameters(tweet, followee);

                localNotification.Notify($"{tweet.CreatedBy.ScreenName} tweeted.", tweet.Text, tweet.Url);

                Tweet.PublishTweet(parameters);
            }
        }

        private bool IsNotRetweetAndReply(ITweet tweet)
        {
            return (!tweet.IsRetweet && (tweet.InReplyToScreenName == null && tweet.InReplyToStatusId == null));
        }

        private PublishTweetParameters CreateReplyParameters(ITweet tweet, Followee followee)
        {
            var tweetParams = new PublishTweetParameters($"@{tweet.CreatedBy.ScreenName} {followee.ReplyBody}")
            {
                InReplyToTweetId = tweet.Id
            };

            if (followee.Photo.IsAttached)
            {
                byte[] imgBytes = followee.Photo.EncodedBase64.ToBitmap().ToByteArray();
                tweetParams.MediaBinaries.Add(imgBytes);
            }

            return tweetParams;
        }
    }
}