﻿using System;
using Android.App;
using Android.Content;

namespace InstaTweet.Services
{
    public class ServiceManager : IServiceManager
    {
        private readonly Context context;

        public ServiceManager()
        {
            context = Application.Context;
        }

        public event EventHandler Started;
        public event EventHandler Stopped;

        private void RaiseServiceEvent(EventHandler EventType)
        {
            EventType?.Invoke(this, new EventArgs());
        }

        public void StartService(Intent serviceIntent)
        {
            context.StartService(serviceIntent);

            RaiseServiceEvent(Started);
        }

        public void StopService(Intent serviceIntent)
        {
            context.StopService(serviceIntent);

            RaiseServiceEvent(Stopped);
        }
    }
}