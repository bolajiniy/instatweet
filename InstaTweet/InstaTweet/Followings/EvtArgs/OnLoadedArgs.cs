﻿using System;

namespace InstaTweet.Followings.EvtArgs
{
    public class OnLoadedArgs : EventArgs
    {
        public int Count { get; set; }
    }
}