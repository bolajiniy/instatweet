﻿using System;

namespace InstaTweet.Followings.EvtArgs
{
    public class OnExportArgs : EventArgs
    {
        public string Location { get; set; }
    }
}