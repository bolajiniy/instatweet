﻿using InstaTweet.Followings.EvtArgs;
using InstaTweet.Models;
using System;
using System.Collections.Generic;

namespace InstaTweet.Followings
{
    public class FollowingsManager
    {
        public event EventHandler<OnExportArgs> OnExported;
        public event EventHandler<OnImportedArgs> OnImported;
        public event EventHandler<OnLoadedArgs> OnLoaded;

        private readonly IManagerHandler handler;

        public FollowingsManager(IManagerHandler handler)
        {
            this.handler = handler;

            this.handler.OnExported += Handler_OnExported;
            this.handler.OnImported += Handler_OnImported;
            this.handler.OnLoaded += Handler_OnLoaded;
        }

        private void Handler_OnLoaded(object sender, OnLoadedArgs e)
        {
            OnLoaded?.Invoke(this, e);
        }

        private void Handler_OnImported(object sender, OnImportedArgs e)
        {
            OnImported?.Invoke(this, e);
        }

        private void Handler_OnExported(object sender, OnExportArgs e)
        {
            OnExported?.Invoke(this, e);
        }

        public void ExportToDocuments(List<Followee> followees)
        {
            handler.DoExportToDocuments(followees);
        }

        public void ImportFromDocuments(out List<Followee> followees)
        {
            handler.DoImportFromDocuments(out followees);
        }

        public void Initialize(string screenName)
        {
            handler.DoInitialize(screenName);
        }

        public void LoadFromAppData(ref List<Followee> followees)
        {
            handler.DoLoadFromAppData(ref followees);
        }

        public void SaveToAppData(List<Followee> followees)
        {
            handler.DoSaveToAppData(followees);
        }
    }
}