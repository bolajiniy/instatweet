﻿using InstaTweet.Followings.Worker;

namespace InstaTweet.Followings
{
    public class FollowingsFactory
    {
        public static FollowingsFactory Instance { get; } = new FollowingsFactory();

        public FollowingsManager FollowingsManager { get; }

        private FollowingsFactory()
        {
            FollowingsManager = new FollowingsManager(new JsonHandler());
        }
    }
}