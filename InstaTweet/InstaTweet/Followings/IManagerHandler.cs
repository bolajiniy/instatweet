﻿using InstaTweet.Followings.EvtArgs;
using InstaTweet.Models;
using System;
using System.Collections.Generic;

namespace InstaTweet.Followings
{
    public interface IManagerHandler
    {
        event EventHandler<OnExportArgs> OnExported;
        event EventHandler<OnImportedArgs> OnImported;
        event EventHandler<OnLoadedArgs> OnLoaded;

        void DoInitialize(string screenName);
        void DoLoadFromAppData(ref List<Followee> followees);
        void DoSaveToAppData(List<Followee> followees);
        void DoImportFromDocuments(out List<Followee> followees);
        void DoExportToDocuments(List<Followee> followees);
    }
}