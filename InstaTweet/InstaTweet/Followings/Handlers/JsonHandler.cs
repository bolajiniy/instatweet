﻿using InstaTweet.Common;
using InstaTweet.Followings.EvtArgs;
using InstaTweet.Models;
using System;
using System.Collections.Generic;
using System.IO;
using Tweetinvi;

namespace InstaTweet.Followings.Worker
{
    public class JsonHandler : IManagerHandler
    {
        private string _jsonPathAppDocu;
        private string _jsonPathAppData;

        public event EventHandler<OnExportArgs> OnExported;
        public event EventHandler<OnImportedArgs> OnImported;
        public event EventHandler<OnLoadedArgs> OnLoaded;

        public void DoExportToDocuments(List<Followee> followees)
        {
            File.WriteAllText(_jsonPathAppDocu, followees.ToJson());

            var args = new OnExportArgs
            {
                Location = _jsonPathAppDocu
            };

            OnExported?.Invoke(this, args);
        }

        public void DoImportFromDocuments(out List<Followee> followees)
        {
            followees = new List<Followee>();

            if (File.Exists(_jsonPathAppDocu))
            {
                var json = File.ReadAllText(_jsonPathAppDocu);
                followees = json.ConvertJsonTo<List<Followee>>();
            }

            var args = new OnImportedArgs
            {
                Count = followees.Count,
                Location = _jsonPathAppDocu
            };

            OnImported?.Invoke(this, args);
        }

        public void DoInitialize(string screenName)
        {
            _jsonPathAppDocu = Path.Combine(FileSystemHelper.AppDocuDirectory, $"{screenName}.json");
            _jsonPathAppData = Path.Combine(FileSystemHelper.AppDataDirectory, $"{screenName}.json");
        }

        public void DoLoadFromAppData(ref List<Followee> followees)
        {
            if (File.Exists(_jsonPathAppData))
            {
                var json = File.ReadAllText(_jsonPathAppData);
                var list = json.ConvertJsonTo<List<Followee>>();

                if (list.Count > 0)
                {
                    followees = list;
                }

                var args = new OnLoadedArgs
                {
                    Count = list.Count
                };

                OnLoaded?.Invoke(this, args);
            }
        }

        public void DoSaveToAppData(List<Followee> followees)
        {
            File.WriteAllText(_jsonPathAppData, followees.ToJson());
        }
    }
}