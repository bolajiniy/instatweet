﻿
using Android.App;
using InstaTweet.Common;

namespace InstaTweet.Notifications
{
    public sealed class LocalNotificationFactory
    {
        public static LocalNotificationFactory Instance { get; } = new LocalNotificationFactory();

        private ILocalNotification localNotification;

        private LocalNotificationFactory()
        {
            if (ResponderManager.IsOreoOrGreater)
            {
                localNotification = new LocalNotificationO(Application.Context);
            }
            else
            {
                localNotification = new LocalNotification(Application.Context);
            }
        }

        public ILocalNotification GetLocalNotification()
        {
            return localNotification;
        }
    }
}