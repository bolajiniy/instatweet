﻿using Android.App;
using Android.Content;

namespace InstaTweet.Notifications
{
    public class LocalNotificationO : LocalNotificationBase, ILocalNotification
    {
        private const string APP_CHANNEL = "com.dahanware.instatweet";

        private readonly Context context;

        public LocalNotificationO(Context context) : base(context)
        {
            this.context = context;

            CreateNotificationChannel();
        }

        public Notification CreateForegroundNotification()
        {
            return new Notification.Builder(context, APP_CHANNEL)
            .SetContentTitle("Started")
            .SetContentText("Waiting for Tweets...")
            .SetSmallIcon(Resource.Drawable.notification_bar_icon)
            .SetOngoing(true)
            .Build();
        }

        public void Notify(string title, string content = "", string url = "")
        {
            var notification = CreateNotification(title, content, url);

            NotificationManager.Notify(GetUniqueId(), notification);
        }

        private void CreateNotificationChannel()
        {
            var chan = new NotificationChannel(APP_CHANNEL, "InstaTweet", NotificationImportance.Default)
            {
                LockscreenVisibility = NotificationVisibility.Public
            };

            chan.EnableVibration(true);

            NotificationManager.CreateNotificationChannel(chan);
        }

        private Notification CreateNotification(string title, string content, string url)
        {
            var notification = new Notification.Builder(context, APP_CHANNEL)
                .SetContentTitle(title)
                .SetSmallIcon(Resource.Drawable.notification_bar_icon);

            if (content.Length > 0)
                notification.SetContentText(content);

            if (url.Length > 0)
                notification.AddAction(CreateAction(url));

            return notification.Build();
        }

        private Notification.Action CreateAction(string url)
        {
            var intent = new Intent(Intent.ActionView, Android.Net.Uri.Parse(url));

            var pendingIntent = PendingIntent.GetActivity(context, 0, intent, 0);

            return new Notification.Action(0, "Open in Browser.", pendingIntent);
        }
    }
}