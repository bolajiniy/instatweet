﻿using Android.App;
using Android.Content;
using Android.Support.V4.App;

namespace InstaTweet.Notifications
{
    public class LocalNotification : LocalNotificationBase, ILocalNotification
    {
        private readonly Context context;

        public LocalNotification(Context context) : base(context)
        {
            this.context = context;
        }

        public Notification CreateForegroundNotification()
        {
            return new NotificationCompat.Builder(context, null)
                    .SetContentTitle("Started")
                    .SetContentText("Waiting for Tweets...")
                    .SetSmallIcon(Resource.Drawable.notification_bar_icon)
                    .SetOngoing(true)
                    .Build();
        }

        public void Notify(string title, string content = "", string url = "")
        {
            var notification = CreateNotification(title, content, url);

            NotificationManager.Notify(GetUniqueId(), notification);
        }

        private NotificationCompat.Action CreateAction(string url)
        {
            var intent = new Intent(Intent.ActionView, Android.Net.Uri.Parse(url));

            var pendingIntent = PendingIntent.GetActivity(context, 0, intent, 0);

            return new NotificationCompat.Action(0, "Open in Browser.", pendingIntent);
        }

        private Notification CreateNotification(string title, string content, string url)
        {
            var notification = new NotificationCompat.Builder(context, null)
                .SetContentTitle(title)
                .SetSmallIcon(Resource.Drawable.notification_bar_icon);

            if (content.Length > 0)
                notification.SetContentText(content);

            if (url.Length > 0)
                notification.AddAction(CreateAction(url));

            return notification.Build();
        }
    }
}