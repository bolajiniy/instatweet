﻿using Android.App;

namespace InstaTweet.Notifications
{
    public interface ILocalNotification
    {
        Notification CreateForegroundNotification();
        void Notify(string title, string content = "", string url = "");
    }
}