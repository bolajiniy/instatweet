﻿using Android.App;
using Android.Content;
using System;

namespace InstaTweet.Notifications
{
    public abstract class LocalNotificationBase
    {
        private readonly Random random;
        private readonly Context context;

        protected LocalNotificationBase(Context context)
        {
            random = new Random();
            this.context = context;
        }

        protected int GetUniqueId()
        {
            return random.Next(1000, 9000);
        }

        protected NotificationManager NotificationManager => (NotificationManager)context.GetSystemService("notification");
    }
}