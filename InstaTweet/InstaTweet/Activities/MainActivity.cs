﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Views;
using Android.Widget;
using InstaTweet.Common;
using InstaTweet.Credentials;
using InstaTweet.Credentials.EvtArgs;
using InstaTweet.Extensions;
using InstaTweet.Followings;
using InstaTweet.Followings.EvtArgs;
using InstaTweet.Models;
using InstaTweet.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tweetinvi;
using Xamarin.Essentials;

namespace InstaTweet.Activities
{
    [Activity(Theme = "@style/AppTheme", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MainActivity : Activity
    {
        private const int Recv_Followees_Code = 0;

        private ImageButton btnShowPop;
        private ImageView imgProfile;
        private TextView txtScreenName;
        private Button btnStart;
        private Button btnFollowings;
        private TextView txtFolloweesCount;
        private EditText txtPinCode;
        private Button btnVerifyPin;

        private PopupMenu menu;

        private List<Followee> followees;

        private IServiceManager serviceManager;
        private FollowingsManager followingsManager;

        private ProgressDialog PinProgressDialog;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.main_layout);

            initViews();

            initFields();

            initEventHandlers();

            initPinDialog();

            initPopupMenu();

            bool isAuthenticated = Intent.GetBooleanExtra("isAuthenticated", false);

            if (isAuthenticated)
            {
                string screenName = Intent.GetStringExtra("screenName");
                byte[] imgByteArray = Intent.GetByteArrayExtra("imgByteArray");
                HandleSplashIntent(screenName, imgByteArray);
            }
            else
            {
                CredentialsManager.Instance.SetupCredentials();
            }

            btnStart.Text = ResponderManager.IsServiceRunning ? "Stop" : "Start";
        }

        private void initViews()
        {
            btnShowPop = FindViewById<ImageButton>(Resource.Id.showPopup);
            btnShowPop.Click += BtnShowPop_Click;

            imgProfile = FindViewById<ImageView>(Resource.Id.imgProfile);

            txtScreenName = FindViewById<TextView>(Resource.Id.txtScreenName);

            btnStart = FindViewById<Button>(Resource.Id.btnStart);
            btnStart.Click += BtnStart_Click;

            btnFollowings = FindViewById<Button>(Resource.Id.btnFollowings);
            btnFollowings.Click += btnFollowings_Click;

            txtFolloweesCount = FindViewById<TextView>(Resource.Id.txtCount);

            btnVerifyPin = FindViewById<Button>(Resource.Id.btnSavePin);
            btnVerifyPin.Click += BtnVerifyPin_Click;

            txtPinCode = FindViewById<EditText>(Resource.Id.txtPinCode);
        }

        private void initFields()
        {
            followees = new List<Followee>();
            serviceManager = ServiceFactory.Instance.ServiceManager;
            followingsManager = FollowingsFactory.Instance.FollowingsManager;
        }

        private void initEventHandlers()
        {
            CredentialsManager.Instance.OnAuthentication += OnAuthentication;
            CredentialsManager.Instance.OnPinValidate += OnPinValidate;
            CredentialsManager.Instance.OnDeleted += OnDeleted;

            serviceManager.Started += ServiceStarted;
            serviceManager.Stopped += ServiceStopped;

            followingsManager.OnLoaded += OnLoadedFollowees;
        }

        private void initPinDialog()
        {
            PinProgressDialog = new ProgressDialog(this);
            PinProgressDialog.Indeterminate = true;
            PinProgressDialog.SetProgressStyle(ProgressDialogStyle.Spinner);
            PinProgressDialog.SetMessage("Verifying Pin, Please Wait...");
            PinProgressDialog.SetCancelable(false);
        }

        private void InitAndLoadFollowees(string screenName)
        {
            followingsManager.Initialize(screenName);
            followingsManager.LoadFromAppData(ref followees);
        }

        private void SetLoggedScreenName(string screenName)
        {
            txtScreenName.Text = $"Signed in - {screenName}";
        }

        private void HideValidationControls()
        {
            txtPinCode.Visibility = ViewStates.Gone;
            btnVerifyPin.Visibility = ViewStates.Gone;
        }

        private async void OnPinValidate(object sender, OnPinValidateArgs e)
        {
            if (e.TwitterCredential == null)
            {
                PinProgressDialog.Dismiss();
                Toast.MakeText(this, "Invalid Pin Code.", ToastLength.Short).Show();
                return;
            }

            Auth.SetCredentials(e.TwitterCredential);

            (sender as CredentialsManager)?.SaveCredentials(e.TwitterCredential);

            var authenticatedUser = await UserAsync.GetAuthenticatedUser();

            HideValidationControls();
            InitAndLoadFollowees(authenticatedUser.ScreenName);
            SetLoggedScreenName(authenticatedUser.ScreenName);

            var imgStream = await authenticatedUser.GetProfileImageStreamAsync();
            await SetProfileImage(imgStream.ToBytes());

            EnableUiControls(true);

            PinProgressDialog.Dismiss();
        }

        private void OnLoadedFollowees(object sender, OnLoadedArgs e)
        {
            txtFolloweesCount.Text = $"{e.Count} Followees in list.";
        }

        private async Task SetProfileImage(byte[] imgByteArray)
        {
            Bitmap imgBitmap = await BitmapFactory.DecodeByteArrayAsync(
                imgByteArray, 0, imgByteArray.Length);
            imgProfile.SetImageBitmap(imgBitmap);
        }

        private async void HandleSplashIntent(string screenName, byte[] imgByteArray)
        {
            EnableUiControls(true);
            HideValidationControls();
            InitAndLoadFollowees(screenName);
            SetLoggedScreenName(screenName);
            await SetProfileImage(imgByteArray);
        }

        private void EnableUiControls(bool enable)
        {
            btnStart.Enabled = enable;
            btnFollowings.Enabled = enable;
        }

        private async void OnAuthentication(object sender, OnAuthenticationArgs e)
        {
            await Browser.OpenAsync(e.AuthenticationUrl);
        }

        private void initPopupMenu()
        {
            menu = new PopupMenu(this, btnShowPop);
            menu.Inflate(Resource.Menu.main_menu);
            menu.MenuItemClick += Menu_MenuItemClick;
        }

        private void BtnShowPop_Click(object sender, EventArgs e)
        {
            menu.Show();
        }

        private void Menu_MenuItemClick(object sender, PopupMenu.MenuItemClickEventArgs e)
        {
            switch (e.Item.ItemId)
            {
                case Resource.Id.Logout:
                    CredentialsManager.Instance.DeleteCredentials();
                    break;
            }
        }

        private void OnDeleted(object sender, OnDeletedArgs e)
        {
            if (e.IsDeleted)
            {
                Toast.MakeText(this, "Credentials already cleared, Please restart InstaTweet.", ToastLength.Short).Show();
            }
            else
            {
                imgProfile.SetImageResource(Resource.Drawable.signed_out_avatar);
                txtScreenName.Text = "Signed Out.";
                EnableUiControls(false);

                if (ResponderManager.IsServiceRunning)
                {
                    StopService(new Intent(this, typeof(ResponderService)));
                    btnStart.Text = "Start";
                }

                Toast.MakeText(this, "Credentials have been cleared, Please restart InstaTweet.", ToastLength.Long).Show();
            }
        }

        private void BtnVerifyPin_Click(object sender, EventArgs e)
        {
            if (txtPinCode.Text.Length > 0)
            {
                PinProgressDialog.Show();
                CredentialsManager.Instance.ValidatePinCode(txtPinCode.Text);
            }
        }

        private void btnFollowings_Click(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(FollowingsActivity));
            StartActivityForResult(intent, Recv_Followees_Code);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (resultCode == Result.Ok)
            {
                switch (requestCode)
                {
                    case Recv_Followees_Code:
                        followees = data.GetStringExtra("followees").ConvertJsonTo<List<Followee>>();
                        txtFolloweesCount.Text = $"{followees.Count} Followees in list.";
                        break;
                }
            }
        }

        private void BtnStart_Click(object sender, EventArgs e)
        {
            if (followees.Count == 0)
            {
                Toast.MakeText(this, "No Followees.", ToastLength.Short).Show();
                return;
            }

            var responderIntent = new Intent(this, typeof(ResponderService));

            if (!ResponderManager.IsServiceRunning)
            {
                responderIntent.PutExtra("followees", followees.ToJson());

                serviceManager.StartService(responderIntent);
            }
            else
            {
                serviceManager.StopService(responderIntent);
            }
        }

        private void ServiceStopped(object sender, EventArgs e)
        {
            btnStart.Text = "Start";
        }

        private void ServiceStarted(object sender, EventArgs e)
        {
            btnStart.Text = "Stop";
        }
    }
}

