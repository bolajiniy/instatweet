﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using InstaTweet.Credentials;
using InstaTweet.Credentials.EvtArgs;
using InstaTweet.Extensions;
using Tweetinvi;

namespace InstaTweet.Activities
{
    [Activity(MainLauncher = true, NoHistory = true, Theme = "@style/Theme.Splash", ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            CredentialsManager.Instance.OnCredentialsSet += OnCredentialsSet;
        }

        protected override void OnResume()
        {
            base.OnResume();

            CredentialsManager.Instance.SetCredentials();
        }

        private async void OnCredentialsSet(object sender, OnSetCredentialsArgs e)
        {
            var intent = new Intent(Application.Context, typeof(MainActivity));

            intent.PutExtra("isAuthenticated", e.IsAuthenticated);

            if (e.IsAuthenticated)
            {
                var authenticatedUser = await UserAsync.GetAuthenticatedUser();

                intent.PutExtra("screenName", authenticatedUser.ScreenName);

                var imgStream = await authenticatedUser.GetProfileImageStreamAsync();

                intent.PutExtra("imgByteArray", imgStream.ToBytes());
            }

            StartActivity(intent);
        }
    }
}