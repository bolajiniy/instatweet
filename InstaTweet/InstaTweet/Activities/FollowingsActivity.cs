﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Text;
using Android.Widget;
using InstaTweet.Adapters;
using InstaTweet.Extensions;
using InstaTweet.Followings;
using InstaTweet.Followings.EvtArgs;
using InstaTweet.Models;
using Plugin.CurrentActivity;
using Plugin.Media;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tweetinvi;
using Tweetinvi.Models;

namespace InstaTweet.Activities
{
    [Activity(ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class FollowingsActivity : Activity
    {
        private ImageButton btnPopup;
        private AutoCompleteTextView txtScreenName;
        private EditText txtReplyContent;
        private ImageView imgPreview;
        private Button btnAddPic;
        private ListView listViewFollowees;
        private Button btnAdd;
        private Button btnRemove;

        private PopupMenu menu;

        private List<Followee> followees;

        private FollowingsManager followingsManager;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.followings_layout);

            initViews();

            initFields();

            initPopupMenu();

            initEventHandlers();

            LoadFollowees();
        }

        private void initViews()
        {
            btnPopup = FindViewById<ImageButton>(Resource.Id.btnPopup);
            btnPopup.Click += BtnPopup_Click; ;

            txtScreenName = FindViewById<AutoCompleteTextView>(Resource.Id.txtScreenName);
            txtScreenName.TextChanged += txtScreenName_TextChanged;

            txtReplyContent = FindViewById<EditText>(Resource.Id.txtReplyContent);

            imgPreview = FindViewById<ImageView>(Resource.Id.imgPreview);
            imgPreview.Click += ImgPreview_Click;

            btnAddPic = FindViewById<Button>(Resource.Id.btnAddPic);
            btnAddPic.Click += BtnAddPic_Click;

            btnAdd = FindViewById<Button>(Resource.Id.btnAdd);
            btnAdd.Click += BtnAdd_Click;

            btnRemove = FindViewById<Button>(Resource.Id.btnRemove);
            btnRemove.Click += BtnRemove_Click;

            var btnSave = FindViewById<Button>(Resource.Id.btnSave);
            btnSave.Click += BtnSave_Click;

            listViewFollowees = FindViewById<ListView>(Resource.Id.listViewFollowees);
            listViewFollowees.ItemClick += LvFollowers_ItemClick;
        }

        private void initFields()
        {
            followees = new List<Followee>();
            followingsManager = FollowingsFactory.Instance.FollowingsManager;
            CrossCurrentActivity.Current.Activity = this;
        }

        private void initPopupMenu()
        {
            menu = new PopupMenu(this, btnPopup);
            menu.Inflate(Resource.Menu.followings_menu);
            menu.MenuItemClick += Menu_MenuItemClick;
        }

        private void initEventHandlers()
        {
            followingsManager.OnExported += OnExportedFollowees;
            followingsManager.OnImported += OnImportedFollowees;
            followingsManager.OnLoaded += OnLoadedFollowees;
        }

        private void LoadFollowees()
        {
            followingsManager.LoadFromAppData(ref followees);
        }

        private void OnLoadedFollowees(object sender, OnLoadedArgs e)
        {
            if (e.Count > 0)
            {
                UpdateListView(false);
            }
        }

        private void OnImportedFollowees(object sender, OnImportedArgs e)
        {
            if (e.Count > 0)
            {
                Toast.MakeText(this, $"{e.Count} followees imported from {e.Location}.", ToastLength.Short).Show();

                UpdateListView(false);
            }
            else
            {
                Toast.MakeText(this, "No followees have been imported.", ToastLength.Short).Show();
            }
        }

        private void OnExportedFollowees(object sender, OnExportArgs e)
        {
            Toast.MakeText(this, $"Followees exported to {e.Location}", ToastLength.Short).Show();
        }

        private async void BtnAddPic_Click(object sender, EventArgs e)
        {
            var status = await CrossPermissions.Current.CheckPermissionStatusAsync<StoragePermission>();

            if (status != PermissionStatus.Granted)
            {
                status = await CrossPermissions.Current.RequestPermissionAsync<StoragePermission>();
            }

            if (status == PermissionStatus.Granted)
            {
                var selectedPhoto = await CrossMedia.Current.PickPhotoAsync();

                if (selectedPhoto == null)
                {
                    imgPreview.Tag = null;
                    return;
                }

                var streamPhoto = selectedPhoto.GetStream();

                var decodedPhoto = await BitmapFactory.DecodeStreamAsync(streamPhoto);

                imgPreview.SetImageBitmap(decodedPhoto);
                imgPreview.Tag = decodedPhoto;
            }
        }

        private async void txtScreenName_TextChanged(object sender, TextChangedEventArgs e)
        {
            string screenName = e.Text.ToString();

            if (screenName.Length < 1)
            {
                return;
            }

            (sender as AutoCompleteTextView).Adapter = new ArrayAdapter<string>
            (
                this,
                Android.Resource.Layout.SimpleDropDownItem1Line,
                await GetAutoCompleteOptionsAsync(screenName)
            );
        }

        private async Task<List<string>> GetAutoCompleteOptionsAsync(string text)
        {
            var screenNames = new List<string>();

            var users = await SearchAsync.SearchUsers(text, 5);

            foreach (IUser user in users)
            {
                screenNames.Add(user.ScreenName);
            }

            return screenNames;
        }

        private void BtnPopup_Click(object sender, EventArgs e)
        {
            menu.Show();
        }

        private void Menu_MenuItemClick(object sender, PopupMenu.MenuItemClickEventArgs e)
        {
            switch (e.Item.ItemId)
            {
                case Resource.Id.Export_Followees:
                    if (followees.Count > 0)
                        followingsManager.ExportToDocuments(followees);
                    break;
                case Resource.Id.Import_Followees:
                    followingsManager.ImportFromDocuments(out followees);
                    break;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            followingsManager.SaveToAppData(followees);

            var intent = new Intent(this, typeof(MainActivity));
            intent.PutExtra("followees", followees.ToJson());
            SetResult(Result.Ok, intent);

            Finish();
        }

        private int index;

        private void LvFollowers_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            index = e.Position;

            if (index > -1)
            {
                var followee = followees[index];

                txtScreenName.Text = followee.ScreenName;
                txtReplyContent.Text = followee.ReplyBody;

                if (followee.Photo.IsAttached)
                {
                    var imgBitmap = followee.Photo.EncodedBase64.ToBitmap();
                    imgPreview.SetImageBitmap(imgBitmap);
                }
            }
        }

        private void BtnRemove_Click(object sender, EventArgs e)
        {
            if ((index > -1) && (followees.Count > 0))
            {
                followees.RemoveAt(index);
                index = -1;
                UpdateListView(false);
            }
        }

        private async void BtnAdd_Click(object sender, EventArgs e)
        {
            if (followees.Exists(x => x.ScreenName.Equals(txtScreenName.Text)))
            {
                Toast.MakeText(this, $"{txtScreenName.Text} already in list.", ToastLength.Short).Show();
                return;
            }

            try
            {
                var followee = new Followee
                {
                    ScreenName = txtScreenName.Text,
                    ReplyBody = txtReplyContent.Text,
                    Photo = new Photo(),
                    Id = (await UserAsync.GetUserFromScreenName(txtScreenName.Text)).Id
                };

                if (imgPreview.Tag != null)
                {
                    followee.Photo.EncodedBase64 = ((Bitmap)imgPreview.Tag).ToBase64();
                }

                followees.Add(followee);

                UpdateListView(true);
            }
            catch
            {
                Toast.MakeText(this, $"{txtScreenName.Text} not found.", ToastLength.Short).Show();
            }
        }

        private void ImgPreview_Click(object sender, EventArgs e)
        {
            imgPreview.Tag = null;
            imgPreview.SetImageResource(Resource.Drawable.img_preview);
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        private void UpdateListView(bool clearBoxes)
        {
            void ClearUiElements()
            {
                txtScreenName.Text = "";
                txtReplyContent.Text = "";
                imgPreview.Tag = null;
                imgPreview.SetImageResource(Resource.Drawable.img_preview);
            }

            listViewFollowees.Adapter = new FolloweesAdapter(followees, this);

            if (clearBoxes)
            {
                ClearUiElements();
            }
        }
    }
}