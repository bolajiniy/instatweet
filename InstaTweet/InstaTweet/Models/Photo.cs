﻿namespace InstaTweet.Models
{
    public class Photo
    {
        private string _encodedBase64;
        public string EncodedBase64
        {
            get
            {
                return _encodedBase64;
            }

            set
            {
                _encodedBase64 = value;
                IsAttached = true;
            }
        }
        public bool IsAttached { get; set; } = false;
    }
}