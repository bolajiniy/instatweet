﻿namespace InstaTweet.Models
{
    public class Followee
    {
        public long Id { get; set; }
        public string ScreenName { get; set; }
        public string ReplyBody { get; set; }
        public Photo Photo { get; set; }
    }
}