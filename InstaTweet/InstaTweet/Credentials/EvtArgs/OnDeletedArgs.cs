﻿using System;

namespace InstaTweet.Credentials.EvtArgs
{
    public class OnDeletedArgs : EventArgs
    {
        public bool IsDeleted { get; set; }
    }
}