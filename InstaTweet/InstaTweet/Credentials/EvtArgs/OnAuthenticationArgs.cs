﻿using System;

namespace InstaTweet.Credentials.EvtArgs
{
    public class OnAuthenticationArgs : EventArgs
    {
        public string AuthenticationUrl { get; set; }
    }
}