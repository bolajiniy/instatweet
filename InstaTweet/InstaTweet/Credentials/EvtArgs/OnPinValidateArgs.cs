﻿using System;
using Tweetinvi.Models;

namespace InstaTweet.Credentials.EvtArgs
{
    public class OnPinValidateArgs : EventArgs
    {
        public ITwitterCredentials TwitterCredential { get; set; }
    }
}