﻿using System;

namespace InstaTweet.Credentials.EvtArgs
{
    public class OnSetCredentialsArgs : EventArgs
    {
        public bool IsAuthenticated { get; set; }
    }
}