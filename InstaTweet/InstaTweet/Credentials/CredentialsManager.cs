﻿using InstaTweet.Credentials.EvtArgs;
using System;
using System.IO;
using System.Threading.Tasks;
using Tweetinvi;
using Tweetinvi.Models;
using Xamarin.Essentials;

namespace InstaTweet.Credentials
{
    public sealed class CredentialsManager
    {
        private const string FILE_NAME = "credentials.json";

        private readonly string _filePath;

        private IAuthenticationContext _context;

        public event EventHandler<OnAuthenticationArgs> OnAuthentication;
        public event EventHandler<OnPinValidateArgs> OnPinValidate;
        public event EventHandler<OnSetCredentialsArgs> OnCredentialsSet;
        public event EventHandler<OnDeletedArgs> OnDeleted;

        public static CredentialsManager Instance { get; } = new CredentialsManager();

        private CredentialsManager()
        {
            _filePath = Path.Combine(GetInternalPath(), FILE_NAME);
        }

        public void SetupCredentials()
        {
            var consumerCreds = new ConsumerCredentials
            (
                "HlMbFePML1mgpoHF89TmzHYsm",
                "uR4V2g7AWbxHJE9BcXbukBOno1UBZ4KbWEMuOLPI6pIfvgqAK1"
            );

            _context = AuthFlow.InitAuthentication(consumerCreds);

            OnAuthentication?.Invoke(this, new OnAuthenticationArgs
            {
                AuthenticationUrl = _context.AuthorizationURL
            });
        }

        public void SetCredentials()
        {
            var credentials = LoadCredentials();

            bool isAuthenticated = credentials != null;

            if (isAuthenticated)
            {
                Auth.SetCredentials(credentials);
            }

            OnCredentialsSet?.Invoke
            (
                this,
                new OnSetCredentialsArgs
                {
                    IsAuthenticated = isAuthenticated
                }
            );
        }

        public async void ValidatePinCode(string PinCode)
        {
            var userCredentials = await CreateCredentialsFromVerifierCodeAsync(PinCode);

            OnPinValidate?.Invoke
            (
                this,
                new OnPinValidateArgs
                {
                    TwitterCredential = userCredentials
                }
            );
        }

        private async Task<ITwitterCredentials> CreateCredentialsFromVerifierCodeAsync(string PinCode)
        {
            return await Task.Run(() => AuthFlow.CreateCredentialsFromVerifierCode(PinCode, _context));
        }

        public void SaveCredentials(ITwitterCredentials credentials)
        {
            File.WriteAllText(_filePath, credentials.ToJson());
        }

        public ITwitterCredentials LoadCredentials()
        {
            if (File.Exists(_filePath))
            {
                var json = File.ReadAllText(_filePath);
                return json.ConvertJsonTo<TwitterCredentials>();
            }

            return null;
        }

        public void DeleteCredentials()
        {
            OnDeleted?.Invoke(this, new OnDeletedArgs { IsDeleted = !File.Exists(_filePath) });
            File.Delete(_filePath); //If the file does not exist, no exception is thrown.
        }

        private string GetInternalPath()
        {
            return FileSystem.AppDataDirectory;
        }
    }
}