﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace InstaTweet.Extensions
{
    public static class StreamExt
    {
        public static byte[] ToBytes(this Stream Stream)
        {
            using (MemoryStream memStream = new MemoryStream())
            {
                Stream.CopyTo(memStream);
                return memStream.ToArray();
            }
        }
    }
}