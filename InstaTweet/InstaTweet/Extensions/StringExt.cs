﻿using Android.Graphics;
using Android.Util;

namespace InstaTweet.Extensions
{
    public static class StringExt
    {
        public static Bitmap ToBitmap(this string str)
        {
            var decodedString = Base64.Decode(str, Base64Flags.Default);
            return BitmapFactory.DecodeByteArray(decodedString, 0, decodedString.Length);
        }
    }
}