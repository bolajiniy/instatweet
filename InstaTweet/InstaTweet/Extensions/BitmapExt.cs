﻿using Android.Graphics;
using System;
using System.IO;

namespace InstaTweet.Extensions
{
    public static class BitmapExt
    {
        public static string ToBase64(this Bitmap bitmap)
        {
            using (var stream = new MemoryStream())
            {
                bitmap.Compress(Bitmap.CompressFormat.Webp, 0, stream);
                return Convert.ToBase64String(stream.ToArray());
            }
        }

        public static byte[] ToByteArray(this Bitmap bitmap)
        {
            using (var stream = new MemoryStream())
            {
                bitmap.Compress(Bitmap.CompressFormat.Webp, 0, stream);
                return stream.ToArray();
            }
        }
    }
}